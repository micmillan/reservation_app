'use strict';

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    cssmin = require("gulp-cssmin"),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    livereload = require("gulp-livereload"),
    webserver = require("gulp-webserver"),
    remove = require("del");

gulp.task('webserver', function() {
  return gulp.src("./")
		.pipe(webserver({
      host: "localhost",
      port: 7878,
      open: true,
      livereload: true
		}));
});
gulp.task("default", [
	"webserver"
]);
